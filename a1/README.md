# LIS4331

## John Husum

### Assignment # 1:

#### Assignment Screenshots:
*Screenshot of running java Hello*

![JDK Java Hello](/a1/img/hello_java.PNG)

*Screenshot of running Android Studio My First App*

![Android Studio Hello](/a1/img/hello_android.PNG)

*Screenshots of running Android Studio Contacts App*

![Android Studio Contacts](/a1/img/contacts_main.PNG) ![Android Studio Andrew Field](/a1/img/contacts_field1.PNG)

#### Git commands w/short descriptions:
1. git init - creates a new respository
2. git status - list the files thay you've changed
3. git add - add one or more files to the index
4. git commit - save changes to the repository
5. git push - send to the remote repository
6. git pull - fetch from the remote repository
7. git clone - create a copy of the repository

#### Bitbucketrepo links:

[A1](https://bitbucket.org/johnedhus/lis4331/src/master/a1/README.md)

[BitbucketStationLocations](https://bitbucket.org/johnedhus/bitbucketstationlocations/src/master/)