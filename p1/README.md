# LIS4331

## John Husum

### Project #1:

#### Project Screenshots:

| *Screenshot of Splash Screen* | *Screenshot of unpopulated user interface* | *Screenshot of Funtionality* |
|---|---|---|
| ![splash screen](/p1/img/splash.gif)| ![unpopulated screen](/p1/img/screen.png)| ![unpopulated screen](/p1/img/working.gif)|

#### Skillsets Screenshots:

| *SS7 Screenshot* | *SS7 Code* |
|---|---|
|![SS7 Work](/p1/img/ss7work.PNG)|![SS7 Code](/p1/img/ss7code.png ){:width=50%}|

| *SS8 BmiGui3 Screemshot* | *SS8 BmiGui3 Code* |
|---|---|
|![SS8 BmiGui3 Work](/p1/img/ss8BMI.gif)|![SS8 BmiGui3 Code](/p1/img/ss8BMIcode.png){:width=50%}|

| *SS8 Screemshot* | *SS8 Code* |
|---|---|
|![SS8 Work](/p1/img/ss8.gif)|![SS8 Code](/p1/img/ss8code.png =500x){:width=50%}|

| *SS9 Screemshot* | *SS9 Code* |
|---|---|
|![SS9 Work](/p1/img/ss9.gif)|![SS9 Code](/p1/img/ss9code.png =500x){:width=50%}|