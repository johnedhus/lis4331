# LIS4331

## John Husum

### Assignment #5:

#### Assignment Screenshots:

| *Screenshot of the Mainscreen* | *Screenshot of an Individual article* | *Screenshot of the Article Link* |
|---|---|---|
| ![main](/a5/img/main.gif)| ![article](/a5/img/article.gif)| ![browser](/a5/img/browser.gif)|

#### Skillsets Screenshots:

| *SS13 Write.java* | *SS13 Read.java* |
|---|---|
| ![main](/a5/img/ss13_write.gif)| ![article](/a5/img/ss13_read.gif)|

#### *SS13 Write.java code*
 
```java
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

class Write
{
    static String filename;
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);

        System.out.print("Filename: ");
        filename = scan.nextLine();

        File file = new File(filename);

        try
        {
            file.createNewFile();
        }
        catch(IOException e)
        {
            System.out.print("ERROR: could not create file");
        }

        Boolean loop = true;

        System.out.println("\nType '!q' to exit.\n");
        while(loop)
        {
            try
            {
                FileWriter filewriter = new FileWriter(filename, true);

                String line = scan.nextLine();
                if(line.equals("!q") ||  line.equals("!Q"))
                {
                    loop = false;
                }
                else
                {
                    filewriter.append(line);
                    filewriter.append(String.format("%n"));
                    filewriter.close();
                }
            }
            catch(IOException e)
            {
                System.out.println("failed to type to file");
                loop = false;
            }
        }
    }
}
```

#### *SS14 Read.java code*

```java
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

class Read
{
    static String filename;
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);

        System.out.print("Filename: ");
        filename = scan.nextLine();

        try
        {
            File file = new File(filename);
            Scanner reader = new Scanner(file);

            System.out.print("\n");
            while(reader.hasNextLine())
            {
                String line = reader.nextLine();
                System.out.println(line); 
            }
        }
        catch(FileNotFoundException e)
        {
            System.out.println("ERROR: File not found.");
        }
        catch(IOException e)
        {
            System.out.println("ERROR: IO failure.");
        }
    }
}
```


| *SS14 Saved.java* |
|---|
| ![main](/a5/img/ss14_saved.gif)|

#### *SS14 Saved.java code*

```java
import java.util.Scanner;

public class Saved
{
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        boolean test_run = true;
        double prince = 0.0, intrest = 0.0;
        int years = 0;

        //get principal
        while(test_run)
        {
            System.out.print("Enter principal: $");
            try
            {   
                prince = scan.nextDouble();
                test_run = false;
            }
            catch(Exception e)
            {
                System.out.println("Please try again.\n");
                scan.next();
            }
        }

        //get intrest
        test_run = true;
        while(test_run)
        {
            System.out.print("Enter intrest rate: ");
            try
            {   
                intrest = scan.nextDouble();
                test_run = false;
            }
            catch(Exception e)
            {
                System.out.println("Not a valid number!\n");
                scan.next();
            }
        }

        //get years
        test_run = true;
        while(test_run)
        {
            System.out.print("Enter years: ");
            try
            {   
                years = scan.nextInt();
                test_run = false;
            }
            catch(Exception e)
            {
                System.out.println("Not a valid integer!\n");
                scan.next();
            }
        }

        String saved = '$' + Double.toString((prince * intrest * years) / 100 + prince);
        System.out.println("You will have saved " + saved + " in " + years + " years at an intest rate of " + intrest + "%");
    }
}
```

| *SS15 JavaArray.java* |
|---|
| ![main](/a5/img/ss15_array.gif)|

#### *SS15 Saved.java code*

```java
class JavaArray
{
    public static void main(String[] args)
    {
        int[] nums = {12, 15, 34, 67, 4, 9, 10, 7, 13, 50};
        System.out.print("Numbers in the array are ");
        for(int i=0; i < nums.length; i++)
            System.out.print(nums[i] + " ");

        System.out.print("\nNumbers in the reverse order are ");
        for(int j=nums.length-1; j >= 0; j--)
            System.out.print(nums[j] + " ");

        int sum = 0;
        for(int k=0; k < nums.length; k++)
            sum += nums[k];
        System.out.print("\nSum of all numbers is " + sum);

        double avg = (double)sum / nums.length;
        System.out.println("Average is " + avg);

        for(int l=0; l<nums.length; l++)
        {
            if(nums[l] > avg)
                System.out.print(nums[l] + " ");
        }
        System.out.print("are greater than the average");
    }
}
```