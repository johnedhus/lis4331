import java.util.Scanner;

public class TimeConversion{
    
    //constants
    public static final double SECS_IN_MINS=     60.0;
    public static final double MIN_IN_HR=        60.0;
    public static final double HRS_IN_DAY=       24.0;
    public static final double DAYS_IN_WEEK=     7.0;
    public static final double DAYS_IN_YR=       365.0;

    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);

        System.out.print
        (
            "Program converts seconds to minutes, hours, days, weeks, and (regular) years--365 days\n" +
            "1) User integer for secounds (must validate integer input).\n" +
            "2) User printf() function to print (format values per below)\n" +
            "3) Create Java \"constants\" for the following values:\n" +
            "       SECS_IN_MINS,\n" +
            "       MIN_IN_HR,\n" +
            "       HRS_IN_DAY\n" +
            "       DAYS_IN_WEEK,\n" +
            "       DAYS_IN_YR (365 days)\n"
        );

        System.out.print("Please enter number of seconds: ");
        while(!scan.hasNextDouble())
        {
            scan.next();
            System.out.println("Not valid integer!\n");
            System.out.print("Please enter number of seconds: ");
        }
        int sec = scan.nextInt();

        //Print it all
        System.out.printf("%,d second(s) equals\n\n", sec);
        //Sec in Mins
        System.out.printf("%,.2f minutes(s)\n", sec/SECS_IN_MINS);
        //Secs in Hours
        System.out.printf("%,.3f hours(s)\n", sec/SECS_IN_MINS/MIN_IN_HR);
        //Secs in days
        System.out.printf("%,.4f day(s)\n", sec/SECS_IN_MINS/MIN_IN_HR/HRS_IN_DAY);
        //Secs in Week
        System.out.printf("%,.5f week(s)\n", sec/SECS_IN_MINS/MIN_IN_HR/HRS_IN_DAY/DAYS_IN_WEEK);
        //Secs in Year
        System.out.printf("%,.6f year(s)\n", sec/SECS_IN_MINS/MIN_IN_HR/HRS_IN_DAY/DAYS_IN_YR);

        scan.close();
    }
}