import javax.swing.*;

public class GUI extends JFrame
{
    public static void main(String[] args)
    {

        JOptionPane.showMessageDialog(null,
            "Program uses Java GUI message and input dialogs.\n"+
            "Program evaluates integers as even or odd.\n"+
            "Note: Program does *not* perform data validation.");

        String input = JOptionPane.showInputDialog(null, "Enter integer: ");
        int num = Integer.parseInt(input);

        if(num%2 == 0)
        {
            JOptionPane.showMessageDialog(null, num + " is an even number.");
        }
        else
        {
            JOptionPane.showMessageDialog(null, num + " is an odd number.");
        }
        
    }
}
