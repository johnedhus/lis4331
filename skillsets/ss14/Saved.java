import java.util.Scanner;

public class Saved
{
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
        boolean test_run = true;
        double prince = 0.0, intrest = 0.0;
        int years = 0;

        //get principal
        while(test_run)
        {
            System.out.print("Enter principal: $");
            try
            {   
                prince = scan.nextDouble();
                test_run = false;
            }
            catch(Exception e)
            {
                System.out.println("Please try again.\n");
                scan.next();
            }
        }

        //get intrest
        test_run = true;
        while(test_run)
        {
            System.out.print("Enter intrest rate: ");
            try
            {   
                intrest = scan.nextDouble();
                test_run = false;
            }
            catch(Exception e)
            {
                System.out.println("Not a valid number!\n");
                scan.next();
            }
        }

        //get years
        test_run = true;
        while(test_run)
        {
            System.out.print("Enter years: ");
            try
            {   
                years = scan.nextInt();
                test_run = false;
            }
            catch(Exception e)
            {
                System.out.println("Not a valid integer!\n");
                scan.next();
            }
        }

        String saved = '$' + Double.toString((prince * intrest * years) / 100 + prince);
        System.out.println("You will have saved " + saved + " in " + years + " years at an intest rate of " + intrest + "%");
    }
}
