import java.util.Scanner;

public class Travel
{

    public static void main(String[] args)
    {
        //declare vars
        Scanner scan = new Scanner(System.in);
        double miles = 0, MPH = 0;
        boolean test_run = true;
        
        //print stuff
        System.out.println("Program Calculates apporximate travel time\n");
            
        //get Miles
        while(test_run)
        {
            System.out.print("Please enter number of miles: ");
            try
            {   
                miles = scan.nextDouble();
                if(miles > 0 && miles <= 3000)
                    test_run = false;
                else
                    System.out.println("Miles must be greater than 0, and no more than 3000\n");
            }
            catch(Exception e)
            {
                System.out.println("Invalid double--miles must be a number\n");
                scan.next();
            }
        }

        //get MPH, wish I could go over 100
        test_run = true;
        while(test_run)
        {
            System.out.print("Please enter number of MPH: ");
            try
            {   
                MPH = scan.nextDouble();
                if(MPH > 0 && MPH <= 100)
                    test_run = false;
                else
                    System.out.println("MPH must be greater than 0, and no more than 1000\n");
            }
            catch(Exception e)
            {
                System.out.println("Invalid double--MPH must be a number\n");
                scan.next();
            }
        }

        //Caculate travel time and display
        int hours = (int)(miles/MPH);
        int mins = (int)(((int)(miles%MPH)) / MPH * 60);
        System.out.print(hours + " hr(s) " + mins + " Minutes");

    }
}

