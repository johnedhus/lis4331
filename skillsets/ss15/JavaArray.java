class JavaArray
{
    public static void main(String[] args)
    {
        int[] nums = {12, 15, 34, 67, 4, 9, 10, 7, 13, 50};
        System.out.print("Numbers in the array are ");
        for(int i=0; i < nums.length; i++)
            System.out.print(nums[i] + " ");

        System.out.print("\nNumbers in the reverse order are ");
        for(int j=nums.length-1; j >= 0; j--)
            System.out.print(nums[j] + " ");

        int sum = 0;
        for(int k=0; k < nums.length; k++)
            sum += nums[k];
        System.out.print("\nSum of all numbers is " + sum);

        double avg = (double)sum / nums.length;
        System.out.println("Average is " + avg);

        for(int l=0; l<nums.length; l++)
        {
            if(nums[l] > avg)
                System.out.print(nums[l] + " ");
        }
        System.out.print("are greater than the average");
    }
}