import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.ListSelectionModel;
import javax.swing.JScrollPane;

public class MyList extends JFrame
{
    private JList<String> myListJList;
    private JList copyJList;
    private JButton copyJButton;
    private static final String[] MyListNames =
    {"Ryzen 5", "Ryzen 7", "i5", "i7", "8GB Ram", "16GB Ram", "32GB Ram", "RTX 3060ti", "RTX 3070", "RTX 3080"};

    //main
    public static void main(String[] args)
    {
        MyList mylist = new MyList();
        mylist.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mylist.setSize(450, 150);
        mylist.setVisible(true);
    }

    public MyList()
    {
        super("My List");
        setLayout(new FlowLayout());
        myListJList = new JList<>(MyListNames);
        myListJList.setVisibleRowCount(5);
        myListJList.setFixedCellWidth(140);
        myListJList.setFixedCellHeight(15);
        myListJList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);

        add(new JScrollPane(myListJList));
        copyJButton = new JButton("Copy>>>");
        copyJButton.addActionListener
        (
            new ActionListener()
            {
                public void actionPerformed(ActionEvent event)
                {
                    copyJList.setListData(myListJList.getSelectedValues());
                }
            }
        );

        add(copyJButton);
        copyJList = new JList();

        copyJList.setVisibleRowCount(5);
        copyJList.setFixedCellWidth(140);
        copyJList.setFixedCellHeight(15);
        copyJList.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        add(new JScrollPane(copyJList));
    }
}

