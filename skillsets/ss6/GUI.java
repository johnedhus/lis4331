import java.text.DecimalFormat;

import javax.swing.*;

public class GUI extends JFrame
{
    public static void main(String[] args)
    {

        JOptionPane.showMessageDialog(null,
            "Program uses Java GUI message and input dialogs.\n"+
            "Program determines paint cost per room (i.e., \"area\").\n"+
            "For paint \"area\" simplicity: use length x height x 2 + width x height x 2\n"+
            "Format numbers as per bellow: thousand separator,decimal point and $ sign for currency.\n"+
            "Research how many square feet at gallon of paint covers.\n"+
            "Note: Program performs data vaidation"
            );
        String input;

        input = JOptionPane.showInputDialog(null, "Print price per gallon: ");
        float price = Integer.parseInt(input);

        input = JOptionPane.showInputDialog(null, "Length: ");
        float length = Integer.parseInt(input);

        input = JOptionPane.showInputDialog(null, "Width: ");
        float width = Integer.parseInt(input);

        input = JOptionPane.showInputDialog(null, "Height: ");
        float height = Integer.parseInt(input);

        float area = (length*height*2 + width*height*2);

        DecimalFormat df = new DecimalFormat("###,###.##");

        JOptionPane.showMessageDialog(null,
            "Print = $" + df.format(price) + " per gallon.\n"+
            "Area of room = " + df.format(area) + "sq ft.\n"+
            "Total = $" + df.format(area/350*price)
            );
        
    }
}
