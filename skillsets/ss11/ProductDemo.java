import java.util.Scanner;

class ProductDemo
{
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
 
        System.out.println("\n///Bellow are default constructor values:///\n");
        Product prtA = new Product();

        System.out.println("\n///Bellow are user-entered values:///\n");
        System.out.print("Code: ");
        String code = scan.nextLine();
        System.out.print("Description: ");
        String description = scan.nextLine();
        System.out.print("Price: ");
        double price = scan.nextDouble();
        Product prtB = new Product(code, description, price);

        System.out.println("\n///Below using setter methods to pass literal vales, then print() method to display vaules:///\n");
        prtB.setCode("xyz");
        prtB.setDescription("Test Widget");
        prtB.setPrice(89.99);

    }
}