import java.text.DecimalFormat;
import java.text.NumberFormat;

public class Product
{
    private String code;
    private String description;
    private double price;

    public Product()
    {
        code = "abc123";
        description = "My Widget";
        price = 49.99;
        
        System.out.println("Inside the default constructor\n");
        System.out.println("Code = " + code);       
        System.out.println("Description = " + description);
        System.out.println("Price = $" + price);
    }

    public Product(String c, String d, double p)
    {
        this.setCode(c);
        this.setDescription(d);
        this.setPrice(p);

        System.out.println("Inside the default constructor\n");
        System.out.println("Code = " + code);
        System.out.println("Description = " + description);
        System.out.println("Price = $" + price);
    }

    public void setCode(String code)
    {
        if(code.matches("[A-Za-z0-9]+"))
            this.code = code;
        System.out.print("Code: " + code + ", ");
    }

    public void setDescription(String description)
    {
        this.description = description;
        System.out.print("Description: " + description + ", ");
    }

    public void setPrice(double price)
    {
        this.price = price;
        System.out.print("Price: " + price + ", ");
    }

    public String getCode()
    {
        return code;
    }

    public String getDescription()
    {
        return description;
    }

    public double getPrice()
    {
        return price;
    }

}