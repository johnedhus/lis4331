public class Book extends Product
{
    private String author;

    public Book()
    {
        super();
        author = "John Doe";
        System.out.println("Inside the default constructor\n");

    }

    public Book(String c, String d, double p, String a)
    {
        super(c, d, p);

    }

    public void setAuthor(String author)
    {
        this.author = author;
    }

    public String getAuthor()
    {
        return author;
    }

    public void print()
    {
        System.out.print(author);
    }

}
