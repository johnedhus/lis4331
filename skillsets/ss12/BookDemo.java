import java.util.Scanner;

class BookDemo
{
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);

        System.out.println("///Bellow are *derived class* default constructor values:///\n");
        Book bookA = new Book();
        System.out.println("Code = " + BookA.getCode());       
        System.out.println("Description = " + BookA.getDescription());
        System.out.println("Price = $" + BookA.getPrice());
        System.out.println("Author = " + BookA.getAuthor());

        System.out.println("\n///Bellow are *derived class* user-entered values:///\n");
        System.out.print("Code: ");
        String code = scan.nextLine();
        System.out.print("Description: ");
        String description = scan.nextLine();
        System.out.print("Price: ");
        double price = scan.nextDouble();
        System.out.print("Author: ");
        String author = scan.nextLine();
        Product bookB = new Book(code, description, price, author);



    }
}