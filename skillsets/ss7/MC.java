

public class MC{

    public static final double INCHES_TO_CENTIMETER =   2.54;
    public static final double INCHES_TO_METER =        0.0254;
    public static final double INCHES_TO_FOOT =         (1.0/12.0);
    public static final double INCHES_TO_YARD =         (1.0/36.0);
    public static final double FEET_TO_MILE =           (1.0/5280.0);
    
    public static void main(String[] args)
    {
        Scanner scan = new Scanner(System.in);
    
        System.out.println
        (
            "Program conerts inches to centimeters, meters, feet, yards, and miles.\n" +
            "***Notes***\n" +
            "1) Use integer for inches (must validate integer input)\n" +
            "2) Use printf() function to print (fromat values per below output).\n" +
            "3) Create Java \"constents\" for the following values:\n" +
            "       INCHES_TO_CENTIMETER,\n" +
            "       INCHES_TO_METER\n" +
            "       INCHES_TO_FOOT\n" +
            "       INCHES_TO_YARD\n" +
            "       FEET_TO_MILE\n"
        );

        int inches = 0;
        
        //try-catch for valid integer input
        boolean run = true;
        while(run)
        {
            System.out.print("Please enter number of inches: ");
            try
            {   
                inches = scan.nextInt();
                run = false;
            }
            catch(Exception e)
            {
                System.out.println("Not a valid integer!\n");
                scan.next();
            }
        }

        //convert inches to other unites
        System.out.printf("%,d inch(es) equals", inches);
        System.out.printf("%,.6f centimeter(s)\n", inches * INCHES_TO_CENTIMETER);
        System.out.printf("%,.6f meters(s)\n", inches * INCHES_TO_METER);
        System.out.printf("%,.6f feet\n", inches * INCHES_TO_FOOT);
        System.out.printf("%,.6f yards(s)\n", inches * INCHES_TO_YARD);
        System.out.printf("%,.6f miles(s)\n", (inches * INCHES_TO_FOOT * FEET_TO_MILE));
        scan.close();
    }
}



