import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class TriGui implements ActionListener
{
    public static void main(String[] args)
    {
        TriGui gui = new TriGui();
    }

    // onscreen components stored as fields
    private JFrame frame;
    private JTextField AField;
    private JTextField BField;
    private JLabel triLabel;
    private JButton computeButton;

    public TriGui()
    {
        //set up componants
        AField = new JTextField(5);
        BField = new JTextField(5);
        triLabel = new JLabel("Compute Distance Leg C ");
        computeButton = new JButton("Compute");

        // attach GUI as event listener to Compute button
        computeButton.addActionListener(this);

        // layout
        JPanel north = new JPanel(new GridLayout(2, 2));
        north.add(new JLabel("Leg A: "));
        north.add(AField);
        north.add(new JLabel("Leg B: "));
        north.add(BField);

        // overall frame
        frame = new JFrame("Right Triangle");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(new BorderLayout());
        frame.add(north, BorderLayout.NORTH);
        frame.add(triLabel, BorderLayout.CENTER);
        frame.add(computeButton, BorderLayout.SOUTH);
        frame.pack();
        frame.setVisible(true);
    }

    
    public void actionPerformed(ActionEvent event)
    {
        // read A leg and B leg
        String Atext = AField.getText();
        double A = Double.parseDouble(Atext);
        String Btext = BField.getText();
        double B = Double.parseDouble(Btext);

        // Calculate
        double triC = Math.sqrt((A*A) + (B*B));
        triLabel.setText("Leg C: " + triC);
    }
}

