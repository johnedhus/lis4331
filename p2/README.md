# LIS4331

## John Husum

### Project #1:

#### Project Screenshots:

| *Splash Screen* | *Adding Users* | *Updating a User* |
|---|---|---|
| ![splash](/p2/img/splash.gif)| ![add](/p2/img/add.gif)| ![update](/p2/img/update.gif)|

| *View a user* | *Adding Users* |
|---|---|
| ![view](/p2/img/view.gif)| ![delete](/p2/img/delete.gif)|