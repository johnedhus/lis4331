# LIS4331

## John Husum

### Assignment # 3:

#### Assignment Screenshots:

| *Screenshot of Splash Screen* | *Screenshot of unpopulated user interface* |
|---|---|
| ![splash screen](/a3/img/splash.gif)| ![unpopulated](/a3/img/unpopulated.PNG)|
|---|---|

| *Screenshot of Toast Motification* | *Screenshot converted currencyuser interface* |
|---|---|
| ![toast notification](/a3/img/toast.gif) | ![converted](/a3/img/converted.gif) |
|---|---|

#### Skillsets Screenshots:

| *Screenshot of SS4* | *Screenshot of SS4 code* |
|---|---|
| ![ss4](/a3/img/ss4.gif) | ![ss4code](/a3/img/ss4code.PNG) |
|---|---|

| *Screenshot of SS5* | *Screenshot of SS5 code* |
|---|---|
| ![ss5](/a3/img/ss5.gif) | ![ss5code](/a3/img/ss5code.PNG) |
|---|---|

| *Screenshot of SS6* | *Screenshot of SS6 code* |
|---|---|
| ![ss6](/a3/img/ss6.gif) | ![ss6code](/a3/img/ss6code.PNG) |
|---|---|