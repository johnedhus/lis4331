# LIS4331

## John Husum

### Assignment #:

#### Assignment Screenshots:

| *Screenshot of Splash Screen* | *Screenshot of Main* |
|---|---|
| ![splash screen](/a4/img/splash.gif)| ![main screen](/a4/img/main.png)|

| *Screenshot of Incorrect Input* | *Screenshot of Correct Input* |
|---|---|
| ![incorrect](/a4/img/fail.gif)| ![correct](/a4/img/work.gif)|

#### Skillsets Screenshots:

| *SS10 Screenshot* | *SS10 Code* |
|---|---|
|![SS10 code](/a4/img/ss10_code.PNG)|![SS7 Code](/a4/img/ss10Code.gif){:width=50%}|

| *SS11 Class Code* | *SS11 Demo* |
|---|---|
|![SS11 code](/a4/img/ss11_code_class.PNG)|![SS11 demo](/a4/img/ss11work.gif){:width=50%}|

| *SS12 Class Code* | *SS12 Demo* |
|---|---|
|![SS12 code](/a4/img/ss12_code.PNG)|![SS12 demo](){:width=50%}|
