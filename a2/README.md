# LIS4331

## John Husum

### Assignment # 2:

#### Assignment Screenshots:

| *Screenshot of Tip Calculator unpopulated* | *Screenshot of Tip Calculator populated* |
|---|---|
| ![Tip Calculator Empty](/a2/img/scr1.PNG)| ![Tip Calculator Full](/a2/img/scr5.PNG)|
|---|---|
| *Tip Caculator with Number of Guest being 1* | *Example of Code* |
|---|---|
| ![Tip Calculator One](/a2/img/scr6.PNG) | ![Tip Calculator One](/a2/img/code.PNG) |
|---|---|
| *Screenshot of ss1 code* | *Screenshot of ss1 working* |
|---|---|
| ![ss1 code](/a2/img/ss1Code.PNG)| ![ss1 working](/a2/img/ss1Work.PNG)|
|---|---|
| *Screenshot of ss2 code* | *Screenshot of ss2 working* |
|---|---|
| ![ss2 code](/a2/img/ss2Code.PNG)| ![ss2 working](/a2/img/ss2Work.PNG)) |