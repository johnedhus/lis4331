> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4331

## John Husum

### Class Number Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install JDK
    - Install Android Studio
	- Provide screenshots of installations
	- Created a Bitbucket repository

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Completed Tip Calculator Android app
	- Provided screenshots as proof of concept
	- Complete Skillsets 1 to 3
3. [A3 README.md](a3/README.md "My A3 README.md file")
	- Completed Currency Converter Android app
	- Provided screenshots as proof of concept
	- Completed Skillsets 4 to 6

4. [A4 README.md](a4/README.md "My A4 README.md file")
	- Completed Mortgage Calculator Android app
	- Provided screenshots as proof of concept
	- Completed Skillsets 10 to 12
5. [A5 README.md](a5/README.md "My A5 README.md file")
	- Compled the RSS feed Android app
	- Provided screenshots as proof of concept
	- Completed Skillsets 13 to 15

6. [P1 README.md](p1/README.md "My P1 README.md file")
	- Completed MyMusic Android app
	- MyMusic is designed to play music I like
	- Provided screenshots as proof of concept
	- Completed Skillsets 7 to 9
	
7. [P2 README.md](p2/README.md "My P2 README.md file")
	- Completed the MyUsers Android app
	- MyUsers is designed to Add, View, Update, and Delete useres from a database
	- Provided screenshots as proof of concept